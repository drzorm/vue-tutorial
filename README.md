# vue项目搭建


## 环境&语言&依赖要求

- 编辑器: [`vscode`](https://code.visualstudio.com/ "Visual Studio Code")
- 开发环境: [`node`](https://nodejs.org/zh-cn/ "Node.js") >= 7.0.0 [`npm`](https://www.npmjs.com/ "npm") >= 3.0.0
- 语言要求: [`ES6`](http://ES6.ruanyifeng.com/ "部分代码使用了ES6(ECMAScript 6)语法")
- 脚手架工具: [`vue-cli`](https://github.com/vuejs/vue-cli "vue-cli")
- 主要生产依赖: [`vue`](https://cn.vuejs.org/ "vue") [`vue-router`](https://router.vuejs.org/zh-cn/ "vue-router") [~~`vuex`~~](https://vuex.vuejs.org/zh-cn/ "目前便利店项目暂时未使用") [`axios`](https://github.com/axios/axios "axios") ...
- 主要开发依赖: [`webpack`](https://doc.webpack-china.org/) [`Sass`](http://www.css88.com/doc/sass/ "sass") ...


## vscode插件推荐

### 代码类插件: 

- **[`Vetur`](https://marketplace.visualstudio.com/items?itemName=octref.vetur)**: 提供 Vue 语法高亮、错误检查、自动补全等等功能
- **[`Sass`](https://marketplace.visualstudio.com/items?itemName=robinbentley.sass-indented)**: 提供 Sass 语法高亮、代码缩进、自动补全等功能
- **VS Code JavaScript (ES6) snippets**: javascript代码片段
- **IntelliSense for CSS class names**: css class 补全
- **beautify/prettier-vscode**: 代码格式化
- **expand-region**: 扩大选择区域
- **html-snippets**: html代码片段
- **SCSS IntelliSense**: scss补全
- **path-intellisense**: 路径补全
- **Document This**: 生成注释

### 工具类插件: 

- **sublime-keybindings**: sublime text 按键映射
- **tortoise-svn-for-vscode**: SVN版本控制
- **code-settings-sync**: 配置同步
- **vscode-icons**: 文件图标主题
- **Project Manager**: 项目管理
- **Bookmarks**: 书签管理
- [更多](https://marketplace.visualstudio.com/search?target=VSCode&category=Other&sortBy=Downloads)


## Node.js

- Node.js推荐使用7.0.0以上版本([各版本下载地址](https://nodejs.org/zh-cn/download/releases/))
- 安装Node.js同时会自动安装[npm](https://www.npmjs.com/)(Node Package Manager，即node包管理器)，鉴于国内的网络环境，推荐使用[cnpm](https://npm.taobao.org/)来代替npm

```shell
## 全局安装cnpm
$ npm install -g cnpm --registry=https://registry.npm.taobao.org
## 安装生产环境依赖
$ cnpm install --save lodash
## 安装开发环境依赖
$ cnpm install --save-dev lodash
```


## ECMAScript 6.0

- [ECMAScript 6.0](http://es6.ruanyifeng.com/ "ECMAScript 6 入门")（简称 ES6）是 JavaScript 语言的下一代标准，在 2015 年 6 月正式发布。
- ECMAScript 是标准，Javascript 是 ECMAScript 标准的实现。
- 目前主流浏览器(Chrome、Firefox、Edge)最新版本都支持 ES6 语法([≥96%](http://kangax.github.io/compat-table/es6/))。
- 为了兼容不支持 ES6 语法的旧版本浏览器，通常我们在开发时需要将 ES6 通过 JavaScript 编译器([Babel](http://www.css88.com/doc/bable/))转为 ES5。

### [let](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/let) 和 [const](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/const)

let/const，与 var 类似，是 ES6 新增的两个用来声明变量的标示符；let声明普通变量，const 声明常量；大部分情况下可以使用 let/const 来替换 var；

var 与 let/const 的主要区别：

- `var`声明的变量可以使用`var`重复声明，`let/const`不允许重复声明
- `var`是函数作用域，`let/const`是块级作用域
- `var`存在变量提升，`let/const`不存在变量提升

#### 块级作用域

```js
// a-1
{
  let i = 5;
}
alert(i);

// a-2
for (var i = 0; i < 3; i++) {
  setTimeout(function(){
     alert(i);
  });
}

// a-3
for (let i = 0; i < 3; i++) {
  setTimeout(function(){
     alert(i);
  });
}
```

#### 变量提升

```js
// c-1
alert(i);

// c-2
alert(i);
var i = 5;

// c-3
var i;
alert(i);
i = 5;

// c-4
alert(i);
let i = 5;
```

### 箭头函数

ES6 允许使用“箭头”（`=>`）定义函数。

```js
var f = v => v;
```

上面的箭头函数等同于：

```js
var f = function(v) {
  return v;
};
```

箭头函数的一个用处是简化回调函数
```js
// 正常函数写法
[1,2,3].map(function (x) {
  return x * x;
});

// 箭头函数写法
[1,2,3].map(x => x * x);
```

### Module

JavaScript 一直没有模块（module）体系，无法将一个大程序拆分成互相依赖的小文件，再用简单的方法拼装起来，对于开发大型的、复杂的项目形成了巨大障碍。

在 ES6 之前，社区制定了一些模块加载方案，最主要的有 CommonJS([Node.js](https://nodejs.org/zh-cn/ "Node.js")) 、 AMD([require.js](http://requirejs.org/ "RequireJS")) 、CMD([sea.js](https://seajs.github.io/seajs/docs/ "Sea.js"))。

```js
// lib/math.js
export function sum (x, y) { return x + y }
export const pi = 3.141593
```
```js
// a.js
import * as math from "lib/math"
console.log("2π = " + math.sum(math.pi, math.pi))
```
```js
// b.js
import { sum, pi } from "lib/math"
console.log("2π = " + sum(pi, pi))
```

### Sass

- [Sass](http://www.css88.com/doc/sass/) 是对 CSS 的扩展，让 CSS 语言更强大、优雅
- Sass 允许你使用变量、嵌套规则、 mixins、导入等众多功能，相当于让 CSS 拥有可编程的能力
- Sass 完全兼容 CSS 语法


## webpack

[webpack](https://doc.webpack-china.org/) 是一个现代 JavaScript 应用程序的静态模块打包器(module bundler)。

![webpack](http://ww1.sinaimg.cn/large/65b19050gy1fo5it6o0fvj20ts0dd754.jpg)



## Vue

- [Vue](https://cn.vuejs.org/v2/guide/#%E8%B5%B7%E6%AD%A5) (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。
- Vue 的核心库只关注视图层，易于上手，也便于与第三方库或既有项目整合。
- Vue 是前端三大框架(React、Angular、Vue)中容易上手的、中文文档最全面框架。


### vue-router

- [vue-router](https://router.vuejs.org/zh-cn/) 是 Vue 官方的路由插件，它和 vue.js 是深度集成的，适合用于构建单页面应用
- vue 的单页面应用是基于路由和组件的，路由用于设定访问路径，并将路径和组件映射起来
- 传统的页面应用，是用一些超链接来实现页面切换和跳转的
- 在 vue-router 单页面应用中，则是路径之间的切换，也就是组件的切换


## vue-cli

[vue-cli](https://github.com/vuejs/vue-cli) 是 Vue 提供一个官方命令行工具(脚手架)，可用于快速搭建大型单页应用。该工具为现代化的前端开发工作流提供了开箱即用的构建配置。


```shell
# 全局安装 vue-cli
$ npm install -g vue-cli

# 创建一个基于 webpack 模板的新项目
$ vue init webpack vue-tutorial
$ cd vue-tutorial

# 安装依赖
$ npm install

# 使用cnpm安装
# cnpm install

# 启动服务
$ npm run dev

# 打包生产环境代码
$ npm run build
```

├── build/                      # webpack 配置文件  
│   └── ...  
├── config/  
│   ├── index.js                # 项目配置文件  
│   └── ...  
├── src/  
│   ├── main.js                 # app 主入口  
│   ├── App.vue                 # app 主组件  
│   ├── components/             # ui 组件  
│   │   └── ...  
│   └── assets/                 # 资源目录 (由webpack处理)  
│       └── ...  
├── static/                     # 纯静态资源目录 (直接copy)  
├── .babelrc                    # babel 配置  
├── .editorconfig               # 编辑器配置  
├── .postcssrc.js               # postcss 配置  
├── index.html                  # index.html 模版  
├── package.json                # 构建脚本和依赖关系  
└── README.md                   # 项目描述  


### vue示例


```html
<!-- index.html -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>vue-tutorial</title>
  </head>
  <body>
    <div id="app"></div>
    <!-- built files will be auto injected -->
  </body>
</html>

```


```html
<!-- App.vue -->
<!-- 单文件组件 -->
<template>
  <div id="app">
    <img src="https://vuejs.org/images/logo.png">
    <router-view/>
  </div>
</template>

<script>
export default {
  name: 'App'
}
</script>

<style>
#app {
  font-family: 'Avenir'，Helvetica，Arial，sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>
```


```js
/* router.js */
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
})

```


```js
/* main.js */
import Vue from 'vue'
import App from './App'
import router from './router'

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
```


### vue 调试

[vConsole](https://github.com/Tencent/vConsole)

[Chrome 开发者工具中文文档](http://www.css88.com/doc/chrome-devtools)

[Vue Devtools](https://github.com/vuejs/vue-devtools#vue-devtools)

[Vue Devtools 插件下载](https://chrome-extension-downloader.com/036af1e6baa44579269600049e913af8/https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd.crx)


### [前端开发脑图](http://naotu.baidu.com/file/de986560a5546eaf10095f4542bb937a?token=02d39ff3cd6eb73b)

![前端开发脑图](http://ww1.sinaimg.cn/large/65b19050gy1fo5isoilztj20u61uo7bj.jpg)
